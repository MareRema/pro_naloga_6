#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* HexVBin(char hex[])
{
    char bin[24] = "";
    int i = 0;

    for(i=0; hex[i]!='\0'; i++)
    {
        switch(hex[i])
        {
        case '0':
            strcat(bin, "0000");
            break;
        case '1':
            strcat(bin, "0001");
            break;
        case '2':
            strcat(bin, "0010");
            break;
        case '3':
            strcat(bin, "0011");
            break;
        case '4':
            strcat(bin, "0100");
            break;
        case '5':
            strcat(bin, "0101");
            break;
        case '6':
            strcat(bin, "0110");
            break;
        case '7':
            strcat(bin, "0111");
            break;
        case '8':
            strcat(bin, "1000");
            break;
        case '9':
            strcat(bin, "1001");
            break;
        case 'a':
        case 'A':
            strcat(bin, "1010");
            break;
        case 'b':
        case 'B':
            strcat(bin, "1011");
            break;
        case 'c':
        case 'C':
            strcat(bin, "1100");
            break;
        case 'd':
        case 'D':
            strcat(bin, "1101");
            break;
        case 'e':
        case 'E':
            strcat(bin, "1110");
            break;
        case 'f':
        case 'F':
            strcat(bin, "1111");
            break;
        default:
            printf("Invalid hexadecimal input.");
        }
    }
    char *res = malloc(sizeof(char)*strlen(bin));
    strcpy(res, bin);
    return res;
}

void BinVHex(char bin[])
{
    char *a = bin;
    int num = 0;
    do {
        int b = *a=='1'?1:0;
        num = (num<<1)|b;
        a++;
    } while (*a);
    printf("BinVHex: %X\n", num);
    /*char b = (char)num;
    char *res = malloc(sizeof(char)*strlen(&b));
    strcpy(res, &b);
    return res;*/
}

char *UnicodeVUTF8(char stevilo[])
{
    if ((strlen(stevilo) == 8 && stevilo[0] != '1') || (strlen(stevilo) <= 7))
    {
        char resitev[9] = "0xxxxxxx\0";
        int j = strlen(stevilo)-1;
        for (int i = 7; i > 0; i--)
        {
            if (resitev[i] == 'x' && j >= 0)
            {
                resitev[i] = stevilo[j];
                j--;
            }
            else if (resitev[i] == 'x' && j < 0)
            {
                resitev[i] = '0';
                j--;
            }
            //printf("resitev: %s\n", resitev);
            //printf("j: %i\n", j);
        }
        char *res = malloc(8);
        strcpy(res, resitev);
        return res;
    }
    else if ((strlen(stevilo) == 12 && stevilo[0] != '1') || strlen(stevilo) <= 11)
    {
        char resitev[17] = "110xxxxx10xxxxxx\0";
        int j = strlen(stevilo)-1;
        for (int i = 15; i > 2; i--)
        {
            if (resitev[i] == 'x' && j >= 0)
            {
                resitev[i] = stevilo[j];
                j--;
            }
            else if (resitev[i] == 'x' && j < 0)
            {
                resitev[i] = '0';
                j--;
            }
            //printf("resitev: %s\n", resitev);
            //printf("j: %i\n", j);
        }
        char *res = malloc(16);
        strcpy(res, resitev);
        return res;
    }
    else if (strlen(stevilo) <= 16)
    {
        char resitev[25] = "1110xxxx10xxxxxx10xxxxxx\0";
        int j = strlen(stevilo)-1;
        for (int i = 23; i > 3; i--)
        {
            if (resitev[i] == 'x' && j >= 0)
            {
                resitev[i] = stevilo[j];
                j--;
            }
            else if (resitev[i] == 'x' && j < 0)
            {
                resitev[i] = '0';
                j--;
            }
            //printf("resitev: %s\n", resitev);
            //printf("j: %i\n", j);
        }
        char *res = malloc(24);
        strcpy(res, resitev);
        return res;
    }
    else
        return "Neveljavno stevilo!\n";
}

int main(int argc, char **argv)
{
    printf("Vpisano: %s\n", argv[1]);
    
    char *bin = HexVBin(argv[1]);
    printf("HexVBin: %s\n", bin);

    char *utf = UnicodeVUTF8(bin);
    printf("UnicodeVUTF8: %s\n", utf);

    BinVHex(utf);
    /*char *hex = BinVHex(bin);
    printf("BinVHex: %s\n", hex);*/
}
